<?php

class path_profiler_export_ui extends ctools_export_ui {
  /**
* Main entry point to activate a profile.
*/
  function activate_page($js, $input, $item, $step = NULL) {
    drupal_set_title($this->get_page_title('activate', $item));

    $form_state = array(
      'plugin' => $this->plugin,
      'object' => &$this,
      'ajax' => $js,
      'item' => $item,
      'op' => 'activate',
      'form type' => 'activate',
      'rerender' => TRUE,
      'no_redirect' => TRUE,
      'process input' => TRUE,
      'step' => $step,
      // Store these in case additional args are needed.
      'function args' => func_get_args(),
    );

    $output = $this->activate_execute_form($form_state);
    if (!empty($form_state['executed'])) {
      drupal_goto($this->plugin['redirect']['activate']);
    }

    return $output;
  }
  
  /**
   * Execute the form.
   *
   * No wizard functionality necessary for the moment, will re-evaluate later.
   */
  function activate_execute_form(&$form_state) {
    //if (!empty($this->plugin['use wizard'])) {
      //return $this->activate_execute_form_wizard($form_state);
    //}
    //else {
      return $this->activate_execute_form_standard($form_state);
    //}
  }

  /**
   * Execute the standard form for activating path profiles.
   */
  function activate_execute_form_standard(&$form_state) {
    $form = drupal_build_form('path_profiler_activate_item_form', $form_state);
    if ($form_state['executed']) {
      $this->activate_save_form($form_state);
    }

    return $form;
  }
  
  function activate_form(&$form, &$form_state) {
    // Find the menu path.
    $menu = $form_state['plugin']['menu']['menu prefix'] .'/'. $form_state['plugin']['menu']['menu item'] .'/'. $form_state['plugin']['menu']['items']['activate']['path'];
    // Find the profile name.
    $menu = explode('/', $menu);
    $ctools_export_ui = array_search('%ctools_export_ui', $menu);
    $profile = arg($ctools_export_ui);
    $menu[$ctools_export_ui] = arg($ctools_export_ui);
    $form['profile'] = array(
      '#type' => 'value',
      '#value' => $profile,
    );
    $form['info'] = array(
      '#type' => 'markup',
      '#markup' => '<p>' . t('Activating this profile carries a certain amount of risk and assumes that you know what you are doing with a menu alteration of this level. Whitelisted profiles are especially dangerous, proceed with caution.') . '</p>',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Activate'),
    );
    return $form;
  }

  function activate_save_form(&$form_state) {
    $values = $form_state['values'];
    if ($values['op'] == 'Activate') {
      variable_set('path_profiler_active_profile', $values['profile']);
      menu_rebuild();
    }
  }
}

function path_profiler_activate_item_form($form, &$form_state) {
  $form = array();
  $form_state['object']->activate_form($form, $form_state);
  return $form;
}
