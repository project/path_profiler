<?php

$plugin = array(
  'schema' => 'path_profiler',
  'access' => 'administer path profiles',
  'menu' => array(
    'menu prefix' => 'admin/structure',
    'menu item' => 'path_profiler',
    'menu title' => 'Path Profiler', 
    'menu description' => 'Create and test various path profiles.',
    'items' => array(
      'activate' => array(
        'path' => 'list/%ctools_export_ui/activate',
        'title' => 'Activate',
        'page callback' => 'ctools_export_ui_switcher_page',
        'page arguments' => array('path_profiler', 'activate', 5),
        'load arguments' => array('path_profiler'),
        'access callback' => 'ctools_export_ui_task_access',
        'access arguments' => array('path_profiler', 'activate', 5),
        'type' => MENU_CALLBACK,
      ),
    ),
  ),
  'title singular' => t('profile'),
  'title singular proper' => t('Profile'),
  'title plural' => t('profiles'),
  'title plural proper' => t('Profiles'),
  'form' => array(
    'settings' => 'path_profiler_export_ui_form',
    'validate' => 'path_profiler_export_ui_form_validate',
    'submit' => 'path_profiler_export_ui_form_submit',
  ),
  'handler' => array(
    'class' => 'path_profiler_export_ui',
    'parent' => 'ctools_export_ui',
   ),
  'allowed operations' => array(
    'activate'  => array('title' => t('Activate')),
  ),
  'redirect' => array(
    'activate' => 'admin/structure/path_profiler',
  ),
);

function path_profiler_export_ui_form(&$form, &$form_state) {
  $menu = array();
  $form['list_style'] = array(
    '#type' => 'radios',
    '#title' => t('List Type'),
    '#options' => array(
      'black' => t('Blacklist'),
      'white' => t('Whitelist'),
    ),
    '#default_value' => isset($form_state['item']->list_style) ? $form_state['item']->list_style : 'black',
    '#required' => TRUE,
  );
  $form['settings'] = array(
    '#tree' => TRUE,
  );
  foreach (module_implements('menu') as $module) {
    $info = drupal_parse_info_file(drupal_get_path('module', $module) . '/' . $module . '.info');
    $menu = module_invoke($module, 'menu');
    $options = array();
    foreach ($menu as $path => $item) {
      $options[$path]['path'] = l($path, $path);
      $options[$path]['title'] = isset($item['title']) ? $item['title'] : t('N/A');
      $options[$path]['description'] = isset($item['description']) ? $item['description'] : t('N/A');
    }
    $form['settings'][$module] = array(
      '#type' => 'fieldset',
      '#title' => $info['name'],
      '#description' => $info['description'],
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    );
    $form['settings'][$module]['menu'] = array(
      '#type' => 'tableselect',
      '#title' => $info['name'],
      '#description' => $info['description'],
      '#options' => $options,
      '#header' => array('path' => t('Path'), 'title' => t('Title'), 'description' => t('Description')),
      '#default_value' => $form_state['item']->settings[$module]['menu'],
    );
  }
}

function path_profiler_export_ui_form_validate(&$form, &$form_state) {
}

function path_profiler_export_ui_form_submit(&$form, &$form_state) {
}