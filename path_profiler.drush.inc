<?php

/**
 * @file
 *   Feature module drush integration.
 */

/**
 * Implementation of hook_drush_command().
 * 
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function path_profiler_drush_command() {
  $items = array();

  $items['path-profiler-reset'] = array(
    'callback' => 'path_profiler_reset',
    'description' => "Reset your path profile to the default drupal profile.",
  );

  return $items;
}

function path_profiler_reset() {
  variable_set('path_profiler_active_profile', '');
  menu_rebuild();
}